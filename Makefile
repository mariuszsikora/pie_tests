

.PHONY: all
all: default pie pic default_gcc pie_gcc pic_gcc no_pie no_pie_gcc

.PHONY: default
default:
	gcc -c main.c -S -o build_default/main.S
	gcc -c crt0.c -o build_default/crt0.o
	as build_default/main.S -o build_default/main.o
	ld -o build_default/main.out build_default/crt0.o build_default/main.o


.PHONY: pie
pie:
	gcc -c main.c -fPIE -S -o build_pie/main.S
	gcc -c crt0.c -fPIE -o build_pie/crt0.o
	as  build_pie/main.S -o build_pie/main.o
	ld -pie -o build_pie/main.out build_pie/crt0.o build_pie/main.o

.PHONY: no_pie
no_pie:
	gcc -c main.c -no-pie -S -o build_no_pie/main.S
	gcc -c crt0.c -no-pie -o build_no_pie/crt0.o
	as build_no_pie/main.S -o build_no_pie/main.o
	ld -no-pie -o build_no_pie/main.out build_no_pie/crt0.o build_no_pie/main.o

.PHONY: pic
pic:
	gcc -c main.c -fPIC -S -o build_pic/main.S
	gcc -c crt0.c -fPIC -o build_pic/crt0.o
	as build_pic/main.S -o build_pic/main.o
	ld -shared -o build_pic/main.out build_pic/crt0.o build_pic/main.o

.PHONY: default_gcc
default_gcc:
	gcc -o build_default_gcc/main.out -DPRINT main.c

.PHONY: pie_gcc
pie_gcc:
	gcc -fPIE -o build_pie_gcc/main.out -DPRINT main.c

.PHONY: no_pie_gcc
no_pie_gcc:
	gcc -no-pie -o build_no_pie_gcc/main.out -DPRINT main.c

.PHONY: pic_gcc
pic_gcc:
	gcc -fPIC -o build_pic_gcc/main.out -DPRINT main.c

.PHONY: clean
clean:
	rm build_default/*
	rm build_pie/*
	rm build_pic/*
	rm build_default_gcc/*
	rm build_pie_gcc/*
	rm build_pic_gcc/*
	rm build_no_pie/*
	rm build_no_pie_gcc/*
