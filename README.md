
| Machine | default | default-gcc | pie | pie-gcc | pic | pic-gcc | no-pie | no-pie-gcc |
|  -----  |  -----  |  ---------  |  -  |  -----  |  -  |  -----  |  ----  |  --------  |
| Ubuntu 18.04 binutils 2.30 | EXEC | DYN (reloc-addr) | DYN | DYN (reloc-addr) | DYN | DYN (reloc-addr) | EXEC | EXEC (no-reloc-addr) |
