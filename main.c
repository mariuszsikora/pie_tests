
#ifdef PRINT
#include <stdio.h>
#endif


int foo(void) {

    return 10;
}

int glob_a = 11;

void* main(void) {

#ifdef PRINT
    printf("main: %p\n", &main);
#endif

    glob_a += 20;
    foo();

    return 0;
}
